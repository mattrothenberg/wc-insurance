import Vuex from "vuex";

const createStore = () => {
  return new Vuex.Store({
    state: {
      insurance: {
        company: ""
      }
    },
    mutations: {
      setInsuranceCompany(state, company) {
        state.insurance.company = company;
      }
    },
    getters: {
      hasAddedInsurance: state => state.insurance.company !== "",
      insuranceCompany: state => state.insurance.company
    }
  });
};

export default createStore;
