import "../theme/index.css";
import Vue from "vue";
import lang from "element-ui/lib/locale/lang/en";
import locale from "element-ui/lib/locale";

import {
  Alert,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Input,
  Radio,
  RadioGroup,
  RadioButton,
  Checkbox,
  CheckboxButton,
  CheckboxGroup,
  Select,
  Form,
  FormItem,
  Option,
  OptionGroup,
  Button,
  Loading,
  MessageBox,
  Collapse,
  CollapseItem,
  Cascader,
  Table,
  TableColumn,
  Steps,
  Step,
  Upload
} from "element-ui";

export default () => {
  locale.use(lang);
  Vue.use(Alert);
  Vue.use(Dropdown);
  Vue.use(DropdownMenu);
  Vue.use(DropdownItem);
  Vue.use(Input);
  Vue.use(Radio);
  Vue.use(RadioGroup);
  Vue.use(RadioButton);
  Vue.use(Checkbox);
  Vue.use(CheckboxButton);
  Vue.use(CheckboxGroup);
  Vue.use(Select);
  Vue.use(Option);
  Vue.use(OptionGroup);
  Vue.use(Button);
  Vue.use(Loading);
  Vue.use(Collapse);
  Vue.use(CollapseItem);
  Vue.use(Cascader);
  Vue.use(Table);
  Vue.use(TableColumn);
  Vue.use(Form);
  Vue.use(FormItem);
  Vue.use(Steps);
  Vue.use(Step);
  Vue.use(Upload);
  Vue.prototype.$msgbox = MessageBox;
  Vue.prototype.$confirm = MessageBox.confirm;
};
